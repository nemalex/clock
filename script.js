const e = document.documentElement;
const t = document.getElementById('time');
const d = document.getElementById('date');
const body = document.getElementById('body');

body.ondblclick = () => {
	if( window.innerHeight == screen.height) {
		document.exitFullscreen();
	} else {
		e.requestFullscreen();
	}
}

d.onclick = body.ondblclick;

const time = () => {
	const dt = new Date();
	const time = `${pad(dt.getHours())}:${pad(dt.getMinutes())}`
	const date = dt.getFullYear() + '-' + pad(dt.getMonth() + 1) + '-' + pad(dt.getDate());
	t.innerHTML = time;
	d.innerHTML = date;

}

const pad = (dt) => { 
	return (dt < 10 ? '0' : '') + dt;
}

time();
setInterval(time, 30000);
